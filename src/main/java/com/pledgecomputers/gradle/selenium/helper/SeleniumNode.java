package com.pledgecomputers.gradle.selenium.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SeleniumNode extends HashMap<String, Object> {

    /**
     * 
     */
    private static final long serialVersionUID = 5121362873137305413L;
    private static final Pattern PROPERTY_PREFIX_BROWSER = Pattern.compile("^browser([0-9]+)\\.(.*)$");
    
    private List<SeleniumBrowser> browsers = new ArrayList<SeleniumBrowser>();

    public List<SeleniumBrowser> getBrowsers() {
        return browsers;
    }

    public void setBrowsers(List<SeleniumBrowser> browsers) {
        this.browsers = browsers;
    }
    
    public void addProperty(String key, Object value) {
        Matcher browserMatcher = PROPERTY_PREFIX_BROWSER.matcher(key);
        
        if(browserMatcher.matches()){
            Integer browserNum = Integer.parseInt(browserMatcher.group(1));
            String browserProp = browserMatcher.group(2);
            
            while(browsers.size() <= browserNum){
                browsers.add(new SeleniumBrowser());
            }
            
            SeleniumBrowser browser = browsers.get(browserNum);
            browser.put(browserProp, value);
            
        } else {
            this.put(key, value);
        }
    }
    
}
