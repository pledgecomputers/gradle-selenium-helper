package com.pledgecomputers.gradle.selenium.helper;

import java.util.HashMap;

import org.openqa.selenium.remote.DesiredCapabilities;

public class SeleniumBrowser extends HashMap<String, Object> {

    /**
     * 
     */
    private static final long serialVersionUID = 1138873334178355965L;
    
    public DesiredCapabilities toDesiredCapabilities(){
        return new DesiredCapabilities(this);
    }
    
}
