package com.pledgecomputers.gradle.selenium.helper;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class RemoteDriverFactory {
    private static final String PROPERTY_PREFIX_DEFAULT = "selenium";
    private static final String PROPERTY_PREFIX_SITE = "site.";
    private static final Pattern PROPERTY_PREFIX_HUB = Pattern.compile("^hub\\.(.*)$");
    private static final Pattern PROPERTY_PREFIX_NODE = Pattern.compile("^node([0-9]+)\\.(.*)$");
    private static Capabilities[] DEFAULT_CAPABILITIES = new Capabilities[]{
            DesiredCapabilities.chrome()
    };
    
    
    private static Map<String, RemoteDriverFactory> instanceMap = new HashMap<String, RemoteDriverFactory>();
    
    public static void setDefaultCapabilities(Capabilities ... capabilities) {
        DEFAULT_CAPABILITIES = capabilities;
    }
    
    public static RemoteDriverFactory getInstance(String propertyBaseName) {
        RemoteDriverFactory inst = null;
        if(!instanceMap.containsKey(propertyBaseName)){
            inst = new RemoteDriverFactory(propertyBaseName);
            instanceMap.put(propertyBaseName, inst);
        } else {
            inst = instanceMap.get(propertyBaseName);
        }
        return inst;
    }
    
    public static List<RemoteWebDriver> getDrivers() {
        return getDrivers(null);
    }
    
    public static List<RemoteWebDriver> getDrivers(String propertyBaseName) {
        return getInstance(propertyBaseName).getRemoteDrivers();
    }
    
    private String propertyBaseName;
    private Map<String, String> siteProperties = new HashMap<String, String>();
    private SeleniumHub hub = new SeleniumHub();
    private List<SeleniumNode> nodes = new ArrayList<SeleniumNode>();
    
    private RemoteDriverFactory(String propertyBaseName) {
        setPropertyBaseName(propertyBaseName);
    }
    
    private void setPropertyBaseName(String propertyBaseName) {
        if(propertyBaseName==null){
            propertyBaseName=PROPERTY_PREFIX_DEFAULT;
        }
        this.propertyBaseName = propertyBaseName+".";
        parseProperties();
    }
    
    public void reset() {
        siteProperties.clear();
        hub.clear();
        nodes.clear();
    }
    
    public void parseProperties(){
        Properties props = System.getProperties();
        reset();
        
        for (Entry<Object, Object> entry : props.entrySet())
        {
            String key = (String)entry.getKey();
            if(key.startsWith(propertyBaseName)){
                parseProperty(key.substring(propertyBaseName.length()), entry.getValue());
            }
        }
    }
    
    private void parseProperty(String key, Object value) {
        Matcher nodeMatcher = PROPERTY_PREFIX_NODE.matcher(key);
        Matcher hubMatcher = PROPERTY_PREFIX_HUB.matcher(key);
        if(key.startsWith(PROPERTY_PREFIX_SITE)){
            siteProperties.put(key.substring(PROPERTY_PREFIX_SITE.length()), (String)value);
        } else if(hubMatcher.matches()) {
            hub.put(hubMatcher.group(1), value);
        } else if(nodeMatcher.matches()) {
            Integer nodeNum = Integer.parseInt(nodeMatcher.group(1));
            key = nodeMatcher.group(2);
            
            while(nodes.size()<=nodeNum){
                nodes.add(new SeleniumNode());
            }
            
            SeleniumNode node = nodes.get(nodeNum);
            
            node.addProperty(key, value);
        } else {
            // TODO: handle this...
        }
    }
    
    public List<Capabilities> getCapabilities() {
        List<Capabilities> caps = new ArrayList<Capabilities>();
        
        for(SeleniumNode node : nodes){
            for(SeleniumBrowser b:node.getBrowsers()){
                caps.add(b.toDesiredCapabilities());
            }
        }
        
        if(caps.size() == 0){
            caps.addAll(Arrays.asList(DEFAULT_CAPABILITIES));
        }
        
        return caps;
    }
    
    public List<RemoteWebDriver> getRemoteDrivers(){
        List<RemoteWebDriver> drivers = new ArrayList<RemoteWebDriver>();
        List<Capabilities> caps = getCapabilities();
        
        for(Capabilities cap:  caps) {
            try{
                drivers.add(new RemoteWebDriver(new URL(hub.getHubURL()), cap));
            }catch(Exception e){}
        }
        
        return drivers;
    }
    
    public RemoteWebDriver getFromCapability(Capabilities cap) {
        try {
            return new RemoteWebDriver(new URL(hub.getHubURL()), cap);
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
    
    public String getSiteProperty(String key) {
        return siteProperties.get(key);
    }
    
}
