package com.pledgecomputers.gradle.selenium.helper;

import java.util.HashMap;

public class SeleniumHub  extends HashMap<String, Object> {
    
    private static final String DEFAULT_HUB_URL = "http://localhost:4444/wd/hub";
    /**
     * 
     */
    private static final long serialVersionUID = -2306063857639366232L;
    
    public String getHubURL(){
        String url = null;
        
        if(this.containsKey("url")){
            url = (String) this.get("url");
        } else {
            url = DEFAULT_HUB_URL;
        }
        
        return url;
    }

}
